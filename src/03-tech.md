# Phone Tech

In order to understand how phones can be compromised and used to facilitate surveillance, we need to have an accurate understanding of how the different technologies used in phones work, such as the phone's hardware, the phone's firmware and operating system, mobile networks, and to some extent the internet at large.
This will help you build a threat model so you can make informed decisions which is preferable to memorizing seemingly arbitrary steps.

## Cellular Networks

Cellular networks take their name from the many overlapping cells of coverage provided by transceiver towers.[^cell-towers]
In urban areas, there is more dense coverage, so a single phone is in contact with more towers.
In suburban and rural areas, there is less overlap, and consequently a phone is in contact with fewer towers.

[^cell-towers]: Not all cellular sites are towers, but using the layperson term suffices.

Network operators can use information about the signal itself to estimate phone locations.
Coarse location can be determined by the angle-of-arrival at the tower or by knowing from which sector[^sector] the signal arrived.
When a phone's distance from multiple towers is simultaneously measured, the network provider can triangulate the phone's location very accurately.[^multilateration]
LTE networks can position a phone's position to within a few tens of meters, and 5G networks are able do this to within 5 meters.
The more towers there are, the more reliably a phone's location can be determined, thus rural triangulation is generally less accurate than urban triangulation.

[^sector]: The cone-shaped area covered by a single antenna.
[^multilateration]: This is called "uplink multilateration."
And as a note, we're using "triangulation" to mean "multilateration" because in this case it's worth trading technical accuracy for understandability.

When phones connect to a cellular network, they send a unique device ID (IMEI[^IMEI]) along with their subscriber's ID (IMSI[^IMSI]).
An IMSI is typically stored on a physical SIM[^SIM] card or eSIM.[^eSIM]
This means that swapping multiple SIM cards between a device or one SIM card between multiple devices can create a hard link between these identities.
A valid SIM or IMSI is not required to make a phone call; these only authenticate the device to the carrier and determine if the device is permitted to make calls or use mobile data.
For example, in most (if not all) regions, emergency services can be called without a SIM.
Removing a SIM card from your phone **does not** prevent tracking.

[^IMEI]: International Mobile Equipment Identity.
[^IMSI]: International Mobile Subscriber Identity.
[^SIM]: Subscriber Identity Module.
[^eSIM]: Embedded-SIM, a chip integrated directly into the device.

## Types of Phones

Most people who say "phone" mean "smart phone," namely one with an operating system and apps that can be installed by users.
A basic phone is the least sophisticated kind of mobile phone, the kinds seen in the early days of widespread mobile phone adoption that can only make phone calls and send SMS messages.
Somewhat rare these days are feature phones.
They are somewhere between smart phones and simple phones.
They may have vendor specific applications such as an email client or internet browser built in.
To differentiate feature phones and basic phones from smart phones, the term "simple phones" is used to describe the two former types.[^dumb-phones]

[^dumb-phones]: Some people use the phrase "dumb phone" to mean either all simple phones or just basic phones, so we are intentionally avoiding this phrase for clarity.

### Smart Phones

<!-- TODO citation for this paragraph
Location Tracking Using Smartphone Accelerometer and Magnetometer Traces: 10.1145/3339252.3340518
https://www.cpomagazine.com/data-privacy/facebooks-use-of-alternate-location-tracking-methods-to-circumvent-apple-privacy-protections-expands-to-accelerometer-data/
-->
Smart phones generally have a location service feature that allows the phone to provide high-accuracy, real-time location data to applications, most notably maps.
The location service uses signals received from GPS[^GPS] or GLONASS[^GLONASS] satellites to triangulate the phone's position.
Most phones use A-GPS[^A-GPS] which combines received cell tower signals, WiFi signals, and even data exchanged over the internet to quickly and more accurately calculate a phone's position.

[^GPS]: Global Positioning System, run by the US Department of Defense.
[^GLONASS]: Global Navigation Satellite System, a GPS alternative run by the Russian Roscosmos.
[^A-GPS]: Assisted GPS.

Smart phones often also contain a compass, accelerometer, gyroscope, and barometer.
Even without GPS or multilateration, measurements from these sensors can be combined to derive a current location from known previous location.

What this means is that even though GPS signals are passively received by a device, use of location services can broadcast a phone's location, and that turning off location services may not be enough to prevent an app or malware on your phone from coarsely determining your location.

### Simple Phones

Many activists believe that using simple phones rather than smart phones is "more secure."
Because a phone without GPS or location service can still be geolocated, simple phones do not offer significant protection from location tracking.
Feature phones typically lack widely available text or voice chat apps, and by definition basic phones have no such capabilities.
This means that only unencrypted SMS and telephone calls are available, and these are susceptible to interception in more ways than if they had client-server or end-to-end encryption.
Basic phones, the seemingly least technologically advanced, may only have 2G capabilities which means that calls and SMS are trivially interceptable with only about €25 worth of consumer-grade equipment.
Further, many of these devices may have hidden internet capabilities that send telemetry data back to manufacturers without users being aware.

Simply put, **simple phones are not more secure** than smart phones against the majority of threats that most activists face.

## Malware

Malware is malicious software.
It is a program that does something you don't want and tries to hide its activities.
Malware created by the State often has the goal of simply surveilling and spreading to other phones or even electronics like WiFi routers.

Old internet security training said that malware is installed by visiting dodgy websites or opening attachments on emails from unknown recipients, and while this is still true, the attack surface of your phone is far larger.
Most, if not all, of your apps poll for notifications or wait for notifications to arrive from Google Play Services and then make requests to the app's servers.
Some malware is zero-click meaning that it requires no user interaction.
As an example, the Pegasus spyware from the NSO Group used a zero-click exploit and targeted activists, journalists, and politicians.
Malware can be installed on our phone even if you only use trusted apps and only (knowingly) accept messages from trusted contacts.

Some malware only stays in your phone's memory while your phone is on and is unable to persist across reboots.
Because of this, some malware will hijack the phone's shutdown routine and do a false shutdown.
Still, periodically rebooting your phone has the potential to clear malware.

If you believe your phone has been compromised, you will need to find a malware specialist who can help you determine this, and you may need to get a new device.
Malware is less common than you think, but don't let its uncommon nature cause you to ignore legitimate warning signs.
State-sponsored malware will not be as readily detectable as low-effort malware, so the common methods may not apply.
Detection is unfortunately not something you can do yourself.

## Operating Systems

One of the most common questions activists ask about smart phones is "Which is more secure, iOS or Android?"
As with all security questions, the answer is "it depends."

Smart phone operating systems (OSes) come in two flavors: iOS for Apple devices and Android for everything else.
iOS is proprietary with private source code.
Android is a base OS with public source code that manufacturers can modify for their devices.
Manufacturers' Android OSes generally have private source code.
In addition, there are many full versions Android maintained by the open source community,[^android-forks] most notably LineageOS.[^cyanogen]
GrapheneOS and CalyxOS are open source Android OSes that have a significant focus on privacy and security.

[^android-forks]: It should be nevertheless noted that when using a community maintained version of Android, one may still send telemetry or be exposed to tracking by Google because of features in the OS or software that's installed like the Google Play App Store or apps like GMail or Google Authenticator.

[^cyanogen]: LineageOS is the successor the popular but discontinued CyanogenMod.

When a phone is powered on, the hardware starts loading the OS using a process where each step verifies the integrity of the software needed for the next step.
This goes by various names such as secure boot or verified boot.
In order to install a custom OS, this verified boot process must be disabled otherwise the hardware would refuse to load the custom OS because it is not cryptographically signed by a trusted key that was included by the original equipment manufacturer.
This allows for the possibility of a malicious OS that could read your data being installed instead of the genuine OS, either by physical access or by malware.
This does not, however, mean that stock OSes are more or less secure than custom OSes.
It means that there is a different risk profile when disabling verified boot and using a custom OS.

When malware is developed, it must target a single application or OS.
Developing malware is expensive and time-consuming, and once malware is deployed, it can be detected and rendered unable to infect new devices by updates to the targeted app or OS.[^malware-reuse]
Because of this, it is more economical to write malware that can target many users.
iOS has a limited number of versions for a limited number of devices, whereas the Android ecosystem is much more diverse.
This means that targeting Android users is less economical and more difficult for adversaries.

[^malware-reuse]: Additionally, malware has the interesting property that when used it can be captured and cloned so that others can reuse it.
This would be like if every time a missile landed on enemy territory, there would be a chance it could be instantly copied and infinitely reproduced, and also that that particular type of missile would be much more likely to be intercepted in the future.
Militaries would be hesitant to fire so many missiles and would need to be much more strategic about their targets.

Our recommendations are as follows:

- For most individuals who are trying to avoid mass surveillance and low-effort hackers, iOS or stock Android are sufficient as they are easiest to use.
- For individuals who are significantly involved in social movements or expect to be individually targeted, at this time we recommend for their organizing and political work that they use GrapheneOS without Google Play Services, use f-droid as the sole app repository, and install only the minimum number of apps required for communications.
- For individuals who have attracted or expect to attract the attention of intelligence agencies, phones should be avoided for everything related to activism.

## Device Encryption

iOS and Android offer the ability to encrypt your personal data.
This goes under various names like Data Protection or Device Encryption.
Phones generally **do not** have device encryption enabled by default.
This feature **must** be enabled by the user either when the phone is set up or later in the settings.
Likewise, the protection against excessive login attempts must also be enabled.

Device encryption implementations generally use a hardware security module (HSM) or a security coprocessor,[^secure-enclave] special chips in the phone that handle encryption,  decryption, and the cryptographic keys used for these operations.
These chips are important because they protect the keys from unauthorized access and tampering.
These chips may impede adversaries from accessing your data, but it is no guarantee.
The tool GrayKey---among others---is capable of exploiting bugs in HSMs, and in some case it can quickly crack the unlock password and decrypt the data.
The HSMs that may be secure today might have new bugs discovered next month, and law enforcement may develop new techniques for recovering data some 5 or 10 years in the future.
Device encryption does a good job preventing your data from being accessed if a chud gets access to your phone of if a cop snatches it during a stop-and-frisk.
It is not likely to withstand concerted efforts from State intelligence agencies like MI5 or the FBI from accessing your data.

[^secure-enclave]: On Apple devices, this chip is called the Secure Enclave.

A high profile example of this when the FBI cracked the password of the mass shooter's phone about a year after the 2015 San Bernardino shooting.
About 5 years after that, it was revealed access to the data was done via a series of exploits against the software in the HSM.
<!-- sources:
     https://www.theverge.com/2016/4/6/11380204/fbi-iphone-attack-san-bernardino-secret
     https://www.theverge.com/2021/4/14/22383957/fbi-san-bernadino-iphone-hack-shooting-investigation
     https://bugzilla.mozilla.org/show_bug.cgi?id=1245528 <== likely the initial bug in the chain -->

Use of device encryption may help protect against data capture, but **the only way to ensure that data does not get into law enforcement's hands is if that data never existed in the first place.**

## VPNs

A virtual private network (VPN), in the context most activists use the term, refers to an application that routes a device's internet traffic to a service whose purpose is to obfuscate the user's web traffic and IP address from network observers or the servers that are being connected to.
When used, VPNs will protect your traffic from snooping on public WiFi networks, and they will hide your IP address from servers you connect to.
They can add some misdirection to investigations and make passive surveillance more difficult, but VPN apps can leak traffic, or you might forget to enable them.
Traffic to and from your VPN provider can be correlated by State intelligence agencies who are able to view all internet traffic, and your VPN can be legally compelled to collect or turn over logs to law enforcement.
VPNs are cheap, they can improve the security in a few ways, but they should not be relied on to provide anonymity against the State.

## IMSI Catchers

An IMSI catcher[^StingRay] is device that spoofs being a legitimate cell tower and induces phones to connect to it thus allowing eavesdropping or the sending of SMS messages or phone calls.
Sometimes this spoofing is detectable, but detection of them should not be relied on.
In some regions they may be deployed without a warrant, in particular during demos.
In part, IMSI catchers work by downgrading the protocol to one with no encryption or one with breakable encryption.
Even though smart phones have preferences for protocols that offer more protection against interception and spoofing, to enable phones to function in regions with only 2G, and because it is part of the GSM standard, smart phones can still be downgraded into using insecure protocols by IMSI catchers.
Phone calls and SMS messages sent and received by smart phones are not robust against interception by IMSI catchers.

[^StingRay]: Often IMSI catchers are referred to by the popular brand name StingRay.

## Faraday Bags

Phones send and receive information using electromagnetic radiation.
This radiation can be blocked by special materials.
Urban legends and some supporting evidence say that signals can be blocked by putting a phone in one or more crisp bags[^crisp-bag] that have foil lining, but this---like many countermeasures---should not be relied on.
A purpose-built Faraday bag can be acquired, and these can be counted on to block phone signals.

[^crisp-bag]: Also known as "chip bags," for the yanks.

If you need to transport phones and ensure that they are not leaking signals, turning them off may not be enough.
Few smart phones can have their batteries removed.
Something leaning on them in your bag might press the power button.
Malware can hijack the shutdown routine and prevent the phone from actually powering off when you try to shut it down.
More importantly, when phones are "off," they are (generally) more accurately described as being in a low power usage mode which is obvious when one notices that "Find My Phone" style apps are still able to locate them.
Placing a powered off phone in a Faraday bag can prevent them from sending signals and will significantly reduce the possibility that the location can be determined.
