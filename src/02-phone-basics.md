# Your Phone and You

Your phone[^phone] is not merely a valuable personal possession.
It is an extension of yourself.
It contains your memories, your knowledge, your private and semi-private thoughts.
It enables you to rapidly look up information and share it with others.
This connectivity and access to knowledge makes us more effective as we pursue our goals.
Phones---to some degree---have also become requirements to function in modern society.
Because of this, individuals are rarely without them.
When its battery dies or we leave home without it we can feel naked, incapacitated, or like some part of us is missing.

[^phone]: To save space, we are using "phone" to mean "mobile phone" or "cell phone."
<!-- TODO note about landlines? -->

Compromise of a phone---either via confiscation or malware---by an adversary can be disastrous.
All your photos, text messages, emails, and notes could be made available to this adversary.
They could have access to all the currently logged-in accounts on your phone.
Installed malware or stalking apps could enable your phone's microphone or real-time location tracking after it's returned to you.

Aside from these types of active surveillance, your phone provides passive surveillance to privileged parties such as the police who can request bulk or real-time access to metadata that is available to your carrier or ISP.[^isp]

[^isp]: Internet service provider

Because of these possibilities of surveillance, activists rightly say "Your phone is a cop" and "Your phone is a snitch."
So then, should we keep using our phones because of what they enable, or should be discard them because of the dangers they pose?
Or, perhaps there's some nuance in when and how we can use phones that allows us to retain much of their benefit while evading much of their detriments.
